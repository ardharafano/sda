<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ditjen SDA - Kementrian PUPR</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sikap Untuk Air Dunia 2023">
    <meta name="keywords" content="Suara, Ditjen SDA, Kemenrian PUPR">

    <meta property="og:title" content="suara.com" />
    <meta property="og:description" content="Sikap Untuk Air Dunia 2023" />
    <meta property="og:url" content="https://www.suara.com/" />
    <meta property="og:image" content="assets/images/navbar/logo.svg" />

    <link rel="Shortcut icon" href="assets/images/navbar/logo.svg">
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/splide.min.css?<?= time() ?>">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css?<?= time() ?>" > -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <script src="assets/js/main.js?<?= time() ?>"></script>

    <!-- tailwind -->
    <link rel="stylesheet" href="assets/css/tailwind.css?<?= time() ?>">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/flowbite.min.js"></script>
    <!-- end tailwind -->

    <!-- humblescroll -->
    <link rel="stylesheet" href="assets/css/humbleScroll.min.css?<?= time() ?>" />
    <script src="assets/js/humbleScroll.js?<?= time() ?>"></script>
    <!-- end humblescroll -->
</head>

<body>

    <?php include('component/navbar.php'); ?>
    <?php
            if(isset($_GET['page'])){ 
                $url = 'index.php?';
                include("pages/".$_GET['page'].".php");
            }else{
                $url = 'index.php?';
                include("pages/home.php");
            }
        ?>
    <?php include('component/footer.php'); ?>

</body>

</html>

<script>
$(document).ready(function() {
    jQuery('img').each(function() {
        jQuery(this).attr('src', jQuery(this).attr('src') + '?' + (new Date()).getTime());
    });
});
</script>


<script>
// Fire regular DSOS
const scroll = new HumbleScroll({
    enableCallback: true,
    repeat: false,
    mirror: false,
    threshold: 0.10,
    offset: {
        top: -64,
        bottom: -150,
    },
})

// Fire custom DSOS with custom options
const myCustomScroll = new HumbleScroll({
    element: '.my-custom-element',
    class: 'enter',
})

const verticalScroll = new HumbleScroll({
    root: document.querySelector('#horizontal-scroll'),
    element: '.my-custom-element',
    class: 'enter',
    repeat: true,
    mirror: true,
})

scroll.on('hs:complete', () => {
    console.log('HumbleScroll is complete!')
})

function firstCall() {
    const card = document.querySelector('#callback-1')
    const cardHeading = document.querySelector('#callback-1-heading')
    card.style = 'background-color: purple; border-radius: 100%; transform: scale(0.75);'
    setTimeout(() => {
        cardHeading.innerHTML = 'Kinda cool right?'
    }, 4000)
}

function secondCall() {
    const section = document.querySelector('#callback-section')
    section.style = 'background-color: #27272a; color: #a3e635;'
}
</script>