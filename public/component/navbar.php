<nav class="font-Montserrat bg-[#DAF8FF] border-gray-200 px-4 sm:px-4 max-lg:py-2.5 py-4 rounded fixed w-full z-20 top-0 left-0"
    style="box-shadow: 0px 8px 12px rgba(0, 0, 0, 0.1);">
    <div class="lg:max-w-[800px] lg:w-full lg:m-auto">

        <div class="container flex flex-wrap items-center justify-between mx-auto">
            <!-- <a href="index.php" class="flex items-center">
                <img src="assets/images/navbar/logo.svg" class="mr-3 w-[106px] h-[-28px]" alt="img">
            </a> -->
            <div>
                <span class="xl:hidden text-sm font-semibold">Ditjen SDA - Kementrian PUPR</span>
            </div>
            <div class="flex md:order-2">

                <button id="dropdownNavbarLink" data-dropdown-toggle="dropdownNavbar" aria-label="button"
                    class="flex items-center justify-between w-full py-2 pl-2 pr-4 font-medium rounded hover:bg-gray-100 md:border-0 md:p-0 md:w-auto xl:text-[#0D2996]">
                    <svg class="w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>

                <div id="dropdownNavbar"
                    class="z-10 hidden font-normal bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700 dark:divide-gray-600">
                    <ul class="" aria-labelledby="dropdownLargeButton">
                        <li>
                            <input type="text" id="search-navbar"
                                class="block w-full p-2 text-sm text-gray-900 border border-gray-300 rounded-lg  focus:ring-blue-500 focus:border-blue-500"
                                placeholder="Search...">
                        </li>
                    </ul>
                </div>

                <button data-collapse-toggle="navbar-sticky" type="button"
                    class="inline-flex items-center p-2 text-sm text-gray-700 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 "
                    aria-controls="navbar-sticky" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-sticky">
                <ul
                    class="font-semibold mt-3 flex flex-col border border-gray-100 rounded-lg  md:flex-row md:space-x-8 xl:space-x-20 md:mt-0 md:text-sm md:font-bold md:border-0 ">
                    <li>
                        <a href="#"
                            class="block py-2 pl-3 pr-4 text-[#0D2996] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0"
                            aria-current="page">HOME</a>
                    </li>
                    <li>
                        <a href="index.php#tentang"
                            class="block py-2 pl-3 pr-4 text-[#0D2996] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 ">TENTANG</a>
                    </li>
                    <li>
                        <a href="index.php#talent"
                            class="block py-2 pl-3 pr-4 text-[#0D2996] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">HADIAH</a>
                    </li>
                    <li>
                        <a href="index.php#event"
                            class="block py-2 pl-3 pr-4 text-[#0D2996] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">JURI</a>
                    </li>
                    <li>
                        <a href="index.php#footer"
                            class="block py-2 pl-3 pr-4 text-[#0D2996] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">SYARAT</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>