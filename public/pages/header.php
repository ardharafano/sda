<div id="header"
    class="font-Montserrat flex justify-center items-center bg-[url('../images/header/bg.png')] bg-cover bg-no-repeat bg-center w-full lg:min-h-[130vh] max-lg:min-h-[100vh] px-4 mt-12">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">
        <div class="flex flex-wrap">
            <div class="lg:w-6/12">
                <div>
                    <img src="assets/images/header/ditjen.svg" class="block w-full max-w-[235px] h-auto my-5 mx-auto">
                    <img src="assets/images/header/title.svg" class="block w-full max-w-[566px] h-auto my-5 mx-auto">
                </div>
                <span class="block text-center font-bold text-lg">#AcceleratingChange</span>
                <a href="#">
                    <div class="bg-[#DA0000] text-white text-center py-4 px-2 rounded-[50px] w-full max-w-[230px] my-10 mx-auto font-bold border-dashed border-2 border-[#fff]">IKUT KOMPETISI</div>
                </a>

                <div>
                    <img src="assets/images/header/tooltip.svg" class="lg:animate-bounce relative lg:-top-64 lg:left-72 max-lg:top-0 max-lg:left-0 w-full max-w-[300px] h-auto block m-auto" alt="img">
                </div>
            </div>

        </div>
    </div>
</div>