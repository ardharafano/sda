<div id="hadiah" class="font-Montserrat  p-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <div class="font-bold text-center">
            <h5 class="text-[#1E1E1E] text-xl mb-4">HADIAH</h5>
            <h3 class="text-[#122FC0] text-3xl mb-4">KOMPETISI</h3>
        </div>
        <div class="flex flex-wrap">
            <div class="lg:w-8/12">
                <div>
                    <img src="assets/images/hadiah/hadiah1.png" alt="img"
                        class="w-full max-w-[800px] h-auto block m-auto">
                </div>
                <div class="w-full max-w-[580px] block m-auto">
                    <p><b>1 Pemenang Utama</b></p>
                    <p>Diwujudkan ide bersama KOL + Uang tunai <b>Rp.3 Juta</b></p>
                </div>
            </div>

            <div class="lg:w-4/12 w-full max-md:mt-5">
                <div>
                    <img src="assets/images/hadiah/hadiah2.png" alt="img"
                        class="w-full max-w-[390px] h-auto block m-auto">
                </div>
                <div class="w-full max-w-[250px] block m-auto">
                    <p><b>2-10 Pemenang Harapan</b></p>
                    <p>Uang Tunai <b>Rp.1 Juta</b></p>
                </div>
            </div>

        </div>
    </div>
</div>