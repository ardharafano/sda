<div id="juri" class="font-Montserrat  p-4 bg-[#0D2996]">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <div class="font-bold text-center">
            <h5 class="text-[#9BB0FF] text-xl mb-4">JURI</h5>
            <h3 class="text-[#FFFFFF] text-3xl mb-4">KOMPETISI</h3>
        </div>
        <div class="flex flex-wrap">
            <div class="lg:w-4/12 w-full">
                <div>
                    <img src="assets/images/juri/ditjen-sda.png" alt="img"
                        class="w-full max-w-[342px] h-auto block m-auto px-5">
                </div>
                <div class="w-full max-w-[250px] block mx-auto mt-5 text-center text-white">
                    <p><b>Perwakilan Ditjen SDA</b></p>
                </div>
            </div>

            <div class="lg:w-4/12 w-full max-md:mt-5">
                <div>
                    <img src="assets/images/juri/kalangsari.png" alt="img"
                        class="w-full max-w-[342px] h-auto block m-auto px-5">
                </div>
                <div class="w-full max-w-[250px] block mx-auto mt-5 text-center text-white">
                    <p><b>Kalangsari Pride</b></p>
                    <p>KOL</p>
                </div>
            </div>

            <div class="lg:w-4/12 w-full max-md:mt-5">
                <div>
                    <img src="assets/images/juri/melati.png" alt="img"
                        class="w-full max-w-[342px] h-auto block m-auto px-5">
                </div>
                <div class="w-full max-w-[250px] block mx-auto mt-5 text-center text-white">
                    <p><b>Melati Wijsen</b></p>
                    <p>Aktivis Lingkungan dan Founder @byebyeplasticbag</p>
                </div>
            </div>

        </div>
    </div>
</div>