<div id="tentang" class="font-Montserrat  px-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">
        <div class="flex flex-wrap">
            <div class="lg:w-6/12 w-full">
                <div>
                    <img src="assets/images/tentang/bg.png" class="absolute left-[-50px] -mt-1 -z-10 max-xl:max-w-[550px] max-xl:w-full">
                </div>
                <div id="fade-up-card" data-hs="fade up">
                    <img src="assets/images/tentang/tentang.png"
                        class="w-full lg:max-w-[433px] max-lg:max-w-[350px] h-auto block m-auto">
                </div>
            </div>


            <div class="lg:w-6/12 w-full py-10" id="fade-up-card" data-hs="fade up">
                <div class="font-bold">
                    <h5 class="text-[#1E1E1E] text-xl mb-4">TENTANG</h5>
                    <h3 class="text-[#122FC0] text-3xl mb-4">Sikap untuk Air Dunia 2023</h3>
                </div>
                <div>
                    <p class="my-5">Air adalah sumber kehidupan, dan pengelolaannya adalah tanggung jawab bersama. Di <b>Hari Air
                            Sedunia 2023</b> ini, ayo ciptakan kesadaran dan partisipasi sebagai bentuk kepedulian pada
                        sumber daya air di sekitar kita.</p>
                    <p class="my-5">Dengan semangat #AcceleratingChange, kita semua bisa menyampaikan gagasan dan kisah inspiratif
                        tentang perubahan yang ingin dilakukan dalam pelestarian sumber daya air di lingkungan terdekat.
                    </p>
                    <p class="my-5">Aku, kamu, dan kita semua siap untuk lakukan perubahan dalam SIKAP nyata: Sadar Lebih Cepat, Aksi
                        Lebih Tanggap!</p>
                </div>
            </div>

        </div>
    </div>
</div>