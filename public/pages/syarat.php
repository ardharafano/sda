<div id="syarat" class="font-Montserrat p-4">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <div class="font-bold text-center">
            <h5 class="text-[#1E1E1E] text-xl mb-4">SYARAT DAN KETENTUAN</h5>
            <h3 class="text-[#122FC0] text-3xl mb-4">KOMPETISI</h3>
        </div>
        <div class="flex flex-wrap bg-[#DAF8FF] rounded-xl p-10">
            <div class="w-full">
                <ol class="list-decimal leading-relaxed">
                    <li>Tema: Sikap | Sadar Lebih Cepat, Aksi Lebih Tanggap</li>
                    <li>Gratis dan terbuka untuk umum, yaitu Warga Negara Indonesia dengan KTP/SIM/Paspor yang masih
                        berlaku.</li>
                    <li>1 Group maksimal berisikan 3 orang.</li>
                    <li>Cerita dan ide tentang perubahan yang ingin dilakukan pada : Sungai , Bendungan, Danau, Rawa,
                        Embung, Irigasi, Pantai.</li>
                    <li>Wajib follow Instagram @pupr_sda</li>
                    <li>Ceritamu dan ide bisa kamu input di microsite.suara.com/sikapuntukhariairdunia2023</li>
                    <li>Cantumkan foto/video sebagai bahan pendukung cerita.</li>
                    <li>Deadline lomba sampai 9 Maret 2023 Pukul 00:59</li>
                    <li>Pemenang utama dengan cerita dan ide terpilih berhak mendapatkan kesempatan untuk diwujudkan
                        berkolaborasi bersama kalangsaripride.</li>
                </ol>
            </div>
        </div>
    </div>
</div>